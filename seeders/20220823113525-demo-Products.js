'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     return queryInterface.bulkInsert('Products', [{
      Name: 'Product-1:',
      description: 'famous for kashmir articles',
      image_url:"image",
      categoryId:1,
      isactive: true,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      Name: 'Product-2: ',
      description: 'famous for kashmir1 articles',
      image_url:"image",
      categoryId:2,
      isactive: true,
      createdAt: new Date(),
      updatedAt: new Date(),

    }, {
      Name: 'Product-3: ',
      description: 'famous for telangana articles',
      image_url:"image",
      categoryId:1,
      isactive: true,
      createdAt: new Date(),
      updatedAt: new Date(),
    },{
      Name: 'Product-4: ',
      description: 'famous for kerala articles',
      image_url:"image",
      categoryId:2,
      isactive: true,
      createdAt: new Date(),
      updatedAt: new Date(),
    }], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     return queryInterface.bulkDelete('Products', null, {});
  }
};
