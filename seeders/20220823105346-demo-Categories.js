'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     return queryInterface.bulkInsert('Categories', [{
      Name: 'Catogories-1',
      description: 'famous for kashmir articles',
      image_url:"http:image",
      groupId:1,
      isactive: true,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      Name: ' Catogories-2:',
      description: 'famous for Telangana articles',
      image_url:"http:image",
      groupId:2,
      isactive: true,
      createdAt: new Date(),
      updatedAt: new Date(),

    }, {
      Name: ' Catogories-3',
      description: 'famous for kerala articles',
      image_url:"http:image",
      groupId:3,
      isactive: true,
      createdAt: new Date(),
      updatedAt: new Date(),
    },{
      Name: ' Catogories-3:',
      description: 'famous for kerala articles',
      image_url:"http:image",
      groupId:4,
      isactive: true,
      createdAt: new Date(),
      updatedAt: new Date(),
    }], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     return queryInterface.bulkDelete('Categories', null, {});
  }
};
