var express = require('express');
var router = express.Router();
var userController=require('../controller/GroupController');



router.get('/groups',userController.getgroups)
router.get('/groups/:groupId/products',userController.activeProducts)
router.get('/groups/products/:id',userController.productsById)







router.delete("/delete/:id", userController.delete);

router.get('/', userController.getAll);

module.exports = router;