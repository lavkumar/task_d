var Model = require('../models');
// module.exports.getAll = function (req, res) {
    // Model.group.findAll()
    //   .then(function (users) {
        // res.send(users);
    //   })
  
//   }
//getting all details of Groups,Products,Categories
module.exports.getAll = (req, res) => {
    Model.Groups.findAll({
        include: {
            model: Model.Categories,
            include: { model: Model.Products }
        }
    })
        .then((user) => {
            res.send(user);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving tutorials."
            });
        });
};

//getting all details of Groups who is active
module.exports.getgroups = (req, res) => {
    Model.Groups.findAll({
        where: { isactive: 'true' }
    })
        .then((user) => {
            res.send(user);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving tutorials."
            });
        });
};

//Get all Active products grouped with categories(only active with name and description)

module.exports.activeProducts = (req, res) => {
    const id = req.params.groupId;
    Model.Groups.findAll({
        attributes: ['Name'], where: { isactive: 'true', id: id },
        include: {
            model: Model.Categories,
            where: { isactive: 'true' },
            attributes: ['Name'],
            include: {
                model: Model.Products,
                where: { isactive: 'true' },
                attributes: ['Name', 'description']
            }
        }
    })
        .then((user) => {
            res.send(user);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving tutorials."
            });
        });
};

//Getting products by id

module.exports.productsById = (req, res) => {
    const id = req.params.id;
    Model.group.findAll({
        where: { isactive: "true" },
        attributes: ['Name'],
        include: {
            model: Model.Categories,
            where: { isactive: "true" },
            attributes: ['Name'],
            include: {
                model: Model.Products,
                where: { isactive: "true", id: id },


            }
        }
    })
        .then((user) => {
            res.send(user);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving tutorials."
            });
        });
};
